package com.jbr540.customerinvoiceapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jbr540.customerinvoiceapi.model.Customer;
import com.jbr540.customerinvoiceapi.service.CustomerService;

@RestController
@CrossOrigin
public class CustomerController {
    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> allCustomers = CustomerService.getAllCustomers();
        return allCustomers ;
    }
}
