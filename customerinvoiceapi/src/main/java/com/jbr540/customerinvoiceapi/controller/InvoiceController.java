package com.jbr540.customerinvoiceapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

import com.jbr540.customerinvoiceapi.model.Invoice;
import com.jbr540.customerinvoiceapi.service.InvoiceService;


@RestController
@CrossOrigin
public class InvoiceController {
    @GetMapping("/invoices")
    public ArrayList<Invoice> getAllInvoices() {
        ArrayList<Invoice> allInvoices = InvoiceService.getAllInvoices();
        return allInvoices;
    }
}
