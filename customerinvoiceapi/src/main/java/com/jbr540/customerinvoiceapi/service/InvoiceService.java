package com.jbr540.customerinvoiceapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.jbr540.customerinvoiceapi.model.Invoice;

@Service
public class InvoiceService {
    private static ArrayList<Invoice> allInvoices = new ArrayList<>();
    static {
        Invoice invoice1 = new Invoice(1,
            CustomerService.getCustomer1(), 30000);
        Invoice invoice2 = new Invoice(2,
            CustomerService.getCustomer2(), 40000);
        Invoice invoice3 = new Invoice(3,
            CustomerService.getCustomer3(), 50000);
        allInvoices.add(invoice1);
        allInvoices.add(invoice2);
        allInvoices.add(invoice3);
    }
    public static ArrayList<Invoice> getAllInvoices() {
        return allInvoices;
    }
}
